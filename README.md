Very basic shellscript for [sdorfehs](https://github.com/jcs/sdorfehs) window manager.
Tested on Ubuntu 18.04 aarch64 (Nintendo Switch)

Change it to suit your system ($BATTERY, echo...)
Run `chmod +x status.sh`
Add `exec status.sh > ~/.config/sdorfehs/bar` to ~/.config/sdorfehs/config