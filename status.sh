#!/bin/sh

while :
sleep 10	# Updating status every 10 seconds
do		# because status takes over sdorfehs messages

DATE=`date +'%a %b %d'`
TIME=`date +'%H:%M'`
BATTERY=`cat /sys/class/power_supply/max170xx_battery/capacity`

# All together now!
echo $DATE \ $TIME \ akku: $BATTERY%
done

